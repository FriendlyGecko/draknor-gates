# Draknor Gates

This project is an attempt to create a simple gate system for players of the game Lands of Draknor. Currently only for systems that have BASH (not default windows :/ ), but am considering transcribing it into powershell.

To contribute new gate locations/provide suggestions for alternative targets please open an issue.